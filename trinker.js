const { filter, forEach } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black 
    },

    allMale: function(p){
        return p.filter((males)=>males.gender=="Male");  
    },

    allFemale: function(p){
        return p.filter((females)=>females.gender=="Female"); 
    },

    nbOfMale: function(p){
        return this.allMale(p).length
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length
    },

    nbOfMaleInterest: function(p){
        return p.filter((maleInterest)=>maleInterest.looking_for=="M").length;
    },

    nbOfFemaleInterest: function(p){
        return p.filter((femaleInterest)=>femaleInterest.looking_for=="F").length;
    },

    // nombre des personnes qui gagnent plus de 2000$

    nbOfPersonnes: function([...p]){
        return p.filter((sommes =>parseInt(sommes.income.substring(1))> 2000)).length;
    },
    // nombre des pers qui aime drama
    nbOfPersonnesLovedDrama: function([...p]){
        return p.filter((prefmovies) =>prefmovies.pref_movie=="Drama").length;
    },
    // nombres des presonne qui aimes qui des films sciences fictions
    nbOfPersonnesLovedScifi: function([...p]){
        return p.filter((prefmovies) =>prefmovies.pref_movie.includes("Sci-Fi")).length;
    },
    // nombres des presonne qui aimes qui des documentaire et gagne plus de  1482 £
    nbOfPersonnesLovedDocumentAndWin1482: function([...p]){
       return p.filter((prefDocument) =>(prefDocument.pref_movie.includes("Documentary") && parseInt(prefDocument.income.substring(1))>1482)).length
     
    },
        //liste des personnes qui gagnent 4000£  
        
        ListeNameFirstnameIdAndRevenuDesPersonnesWhoWin4000: function([...p]){
            let count=[];
         const elemfil= p.filter((list => parseInt(list.income.substring(1))> 4000));
            elemfil.forEach(element => {
                count.push( element.id +"   "+ element.first_name +"   "+ element.last_name + "   " + "gagne"+ "   " + parseInt( element.income.substring(1)))
            });
            return count.length
        },

        personMoreRich: function(p){
            // que les hommes
            // let hommes = this.allMale(p)
            // let tab = hommes.sort(
            //     (a, b) =>
            //     {
            //         return parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1))
            //     })
            // return tab[0]
            //return p

        },
        

        salaireMoyen: function(p){
            let tabl=[];
            let id=[]
            const arraySort = p.filter((prefDocument=> parseFloat(prefDocument.income.substring(1))>0 ))

           
            //arraySort.forEach(element => {
               // tableau.push(element.id +"   "+ element.first_name)
                
                //})
                const fai=arraySort.forEach(element => {
                    tabl.push(parseFloat(element.income.substring(1)))
                    
                    })
                let red= (acc,cur)=> acc+cur
                
                const nbOfid=arraySort.forEach(element=>{
                        id.push(element.id)
                })

                

                //sommes=0;
               //for(let i=1; i< tabl.length;i++){

                //tabl.forEach(element => {
                    //sommes=element.income[i] + sommes
                //});
               //}
               let reverage= tabl.reduce(red)/id.length
                return (reverage.toFixed(2) + "$").yellow
                
            
            },

            salaireMedian: function (p) {
                let tab=[]
                const allsalaire= p.filter((salaire=>parseFloat(salaire.income.substring(1))>0))
                let sorted= (a,b)=> a-b
                allsalaire .forEach(element=>{
                   tab.push(parseInt(element.income.substring(1)))
                   tab.sort(sorted)
                })
                function median(numbers) {
                   // const sorted = numbers.sort((a, b) => a - b);
                   // const middle = Math.floor(sorted.length / 2);
                   let median1=numbers.length/2

                   let median= (numbers[median1]+numbers[median1-1])/2
                
                   //if (sorted.length % 2 === 0){
                     //   return (sorted[middle - 1] + sorted[middle]) / 2;
                    //}
                
                   // return sorted[middle];
                   if (numbers.length % 2 == 0){
                     return median
                   }else{
                    return numbers[median1];
                   }
                   
                }
                return median(tab)
            },

            nbPersonesheminord: function(p){
                return p.filter((number)=>number.latitude>0).length;
            },

            nbPersoneshemisud: function(p){
                return p.filter((number)=>number.latitude<0);
            },

            salaryPersoneshemisud:function ([...p]) {
                    // let tab1=[];
                // let id=[];
               const nbpes=p.filter((salaire)=>salaire.latitude<0 );
               return this.salaireMoyen(nbpes)

                //  nbpes.forEach(element => {
                // tab1.push(parseInt(element.income.substring(1)))

                    
                //  });
                //      nbpes.forEach(element=>{
                //         id.push(parseInt(element.id))
                //     })

                //         let reduc=(acc,rec)=>acc+rec
                
                //          let reverage = tab1.reduce(reduc) / id.length
                        
                       // return reverage

               // return this.salaireMoyen(this.nbPersoneshemisud(p))
            },
            calculateurposition :function calculate(Cawt,position,other){
                let per;
                var result = position(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
                    for (var i=0;i<other.length;i++){ 
                    var ans = position(Cawt[0].latitude,other[i].latitude,Cawt[0].longitude,other[i].longitude);
                    if (ans < result){
                        result = ans;
                        per =other[i];
    
                    }
                }
                return ` "Distance entre eux: " ${result.toFixed(2).yellow} ${per.first_name} ${per.last_name} ${per.id} `.yellow
            },
             personsProchBerenice: function (p) {

            //     const taball= p.filter((allarraywithoutb)=> parseFloat (allarraywithoutb.longitude) !=-87.879523 && parseFloat(allarraywithoutb.latitude)!=15.5900396)

            //     const tabberenice=p.filter((person)=> parseFloat(person.longitude) ==-87.879523 && parseFloat(person.latitude)==15.5900396)

            //     //function dist() {
                    
            //     //}
            //     let lat1= parseInt(tabberenice[0].latitude)
            //     let long1= parseInt(tabberenice[0].longitude)

            //     // function lisalltab (tableau){.longitude=10)
                
            //     let lat2=[]
            //     let long2=[]

            //     function distance(x1, y1, x2, y2) {
            //         return Math.sqrt(sqr(y2 - y1) + sqr(x2 - x1));
            //         } 

            //         //console.log(lisalltab(taball))

            //     // return parseFloat(taball[0].longitude)
            //      taball.forEach(element=>{
            //         lat2.push(parseInt(element.latitude))
                    
            //      })
            //      taball.forEach(element=>{
            //         long2.push(parseInt(element.latitude))
                    
            //      })

            //      function distance(lat1,long1,lat2,long2) {
                     
            //         return Math.sqrt(Math.sqrt(lat1-long1) + Math.sqrt(lat2-long2))
            //      }

            //         return distance()
                let Cawt= []
                let other=[]
                    Cawt = p.filter(cawt =>  cawt.last_name == "Cawt");
                    other = p.filter(cawt =>  cawt.last_name !== "Cawt");
    
    
             function Rad(Value) {
    
                return Value * Math.PI / 180;
            }
    
            function position(lat1,lat2,lng1,lng2){
                globrad = 6372.8; 
                deltaLat = Rad(lat2-lat1);
                deltaLng = Rad(lng2-lng1);
                //console.log(Rad(lat2-lat1))
                //console.log(deltaLat)
                lat1 = Rad(lat1);
                lat2 = Rad(lat2);
                a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
                c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                return  globrad * c;
    
            }
    
    
           
    
            return this.calculateurposition(Cawt,position,other)
               
                
            },

            personsProcheBrach: function ([...p]) {
                let   Brach = p.filter(cawt =>  cawt.last_name == "Brach");
                let  other = p.filter(cawt =>  cawt.last_name !== "Brach");

                function Rad(Value) {
    
                    return Value * Math.PI / 180;
                }


        
                function position(lat1,lat2,lng1,lng2){
                    globrad = 6372.8; 
                    deltaLat = Rad(lat2-lat1);
                    deltaLng = Rad(lng2-lng1);
                    //console.log(Rad(lat2-lat1))
                    //console.log(deltaLat)
                    lat1 = Rad(lat1);
                    lat2 = Rad(lat2);
                    a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
                    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                    return  globrad * c;
        
                }
                
                return this.calculateurposition( Brach,position,other)

 
                
            },
            tenpersonsproch: function (p){
                    let brach=[];
                    let others=[];
                    brach=p.filter(oneperson=>oneperson.last_name=="Boshard")
                    others= p.filter(persons=>persons.last_name!="Boshard")

                    

                    function position(lat1,lat2,lng1,lng2){
                        globrad = 6372.8; 
                        deltaLat = Rad(lat2-lat1);
                        deltaLng = Rad(lng2-lng1);
                        //console.log(Rad(lat2-lat1))
                        //console.log(deltaLat)
                        lat1 = Rad(lat1);
                        lat2 = Rad(lat2);
                        a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
                        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                        return  globrad * c;
            
                    }
                    let Rad = (value)=>value*Math.PI/ 180
                    function calculate(Cawt,position,other){
                        let per;
                        var result = position(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
                            for (var i=0;i<other.length;i++){ 
                            var ans = position(Cawt[0].latitude,other[i].latitude,Cawt[0].longitude,other[i].longitude);
                            if (ans < result){
                                result = ans;
                                per =other[i];
            
                            }
                        }
                        return ` "Distance entre eux " ${result.toFixed(2)}, ${per.first_name} ${per.last_name} ${per.id} `;
                    }
                    return others.length    
            },

            

            personsWhoworkstogoogle: function (p){
                let allpersons=[];
                let nameId=[];
                allpersons=p.filter(oneperson=>oneperson.email.includes("@google"))
                allpersons.forEach(element=>{
                    nameId.push("name: "+element.last_name+"   id: "+ element.id)

                })
            return nameId
            
            },
            olderpeople: function (p){
                    //let dates=p.map((age=>parseInt(age.date_of_birth)==1977-11-06))

                


                var sortBy = (function () {
                    var toString = Object.prototype.toString,
                        // default parser function
                        parse = function (x) { return x; },
                        // gets the item to be sorted
                        getItem = function (x) {
                          var isObject = x != null && typeof x === "object";
                          var isProp = isObject && this.prop in x;
                          return this.parser(isProp ? x[this.prop] : x);
                        };
                  
                    // /**
                    //  * Sorts an array of elements.
                    //  *
                    //  * @param {Array} array: the collection to sort
                    //  * @param {Object} cfg: the configuration options
                    //  * @property {String}   cfg.prop: property name (if it is an Array of objects)
                    //  * @property {Boolean}  cfg.desc: determines whether the sort is descending
                    //  * @property {Function} cfg.parser: function to parse the items to expected type
                    //  * @return {Array}
                    //  */
                    return function sortby (array, cfg) {
                      if (!(array instanceof Array && array.length)) return [];
                      if (toString.call(cfg) !== "[object Object]") cfg = {};
                      if (typeof cfg.parser !== "function") cfg.parser = parse;
                      cfg.desc = !!cfg.desc ? -1 : 1;
                      return array.sort(function (a, b) {
                        a = getItem.call(cfg, a);
                        b = getItem.call(cfg, b);
                        return cfg.desc * (a < b ? -1 : +(a > b));
                      });
                    };
                  
                  }());

                  var sor= sortBy(p, { prop: "date_of_birth" } );

                  return sor[0].first_name 

            },

        youngerPerson: function (p) {
            
            var sortBy = (function () {
                var toString = Object.prototype.toString,
                    // default parser function
                    parse = function (x) { return x; },
                    // gets the item to be sorted
                    getItem = function (x) {
                      var isObject = x != null && typeof x === "object";
                      var isProp = isObject && this.prop in x;
                      return this.parser(isProp ? x[this.prop] : x);
                    };
              
                // /**
                //  * Sorts an array of elements.
                //  *
                //  * @param {Array} array: the collection to sort
                //  * @param {Object} cfg: the configuration options
                //  * @property {String}   cfg.prop: property name (if it is an Array of objects)
                //  * @property {Boolean}  cfg.desc: determines whether the sort is descending
                //  * @property {Function} cfg.parser: function to parse the items to expected type
                //  * @return {Array}
                //  */
                return function sortby (array, cfg) {
                  if (!(array instanceof Array && array.length)) return [];
                  if (toString.call(cfg) !== "[object Object]") cfg = {};
                  if (typeof cfg.parser !== "function") cfg.parser = parse;
                  cfg.desc = !!cfg.desc ? -1 : 1;
                  return array.sort(function (a, b) {
                    a = getItem.call(cfg, a);
                    b = getItem.call(cfg, b);
                    return cfg.desc * (a > b ? -1 : +(a < b));
                  });
                };
              
              }());

              var sor= sortBy(p, { prop: "date_of_birth" } );

              return sor[0].first_name 

            
        },



        match: function(p){
        return "not implemented".red;
    }
}